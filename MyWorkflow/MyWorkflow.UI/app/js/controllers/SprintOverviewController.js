"use strict";

myWorkflow.controller("sprintOverviewController", function ($scope, storyService) {
    $scope.devName = "Chase Hardin";
    $scope.sprints = storyService.sprintData.sprints;
});