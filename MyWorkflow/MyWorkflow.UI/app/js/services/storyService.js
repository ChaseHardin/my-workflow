"use strict";

myWorkflow.factory("storyService", function () {
    return {
        sprintData: {
            sprints: [
                {
                    sprintName: "Sprint 1",
                    stories: [
                        {
                            storyNumber: "AB-100",
                            storyTitle: "Add Creation Page for Promotions",
                            points: 3
                        },
                        {
                            storyNumber: "AB-101",
                            storyTitle: "Add Edit Page for Promotions",
                            points: 2.5
                        }
                    ]
                },
                {
                    sprintName: "Sprint 2",
                    stories: [
                        {
                            storyNumber: "AB-102",
                            storyTitle: "Add Creation Page for Promotions",
                            points: 3
                        },
                        {
                            storyNumber: "AB-103",
                            storyTitle: "Add Edit Page for Promotions",
                            points: 2.5
                        }
                    ]
                },
                {
                    sprintName: "Sprint 3",
                    stories: [
                        {
                            storyNumber: "AB-102",
                            storyTitle: "Add Creation Page for Promotions",
                            points: 3
                        },
                        {
                            storyNumber: "AB-103",
                            storyTitle: "Add Edit Page for Promotions",
                            points: 2.5
                        }
                    ]
                },
                {
                    sprintName: "Sprint 4",
                    stories: [
                        {
                            storyNumber: "AB-102",
                            storyTitle: "Add Creation Page for Promotions",
                            points: 3
                        },
                        {
                            storyNumber: "AB-103",
                            storyTitle: "Add Edit Page for Promotions",
                            points: 2.5
                        }
                    ]
                },
                {
                    sprintName: "Sprint 5",
                    stories: [
                        {
                            storyNumber: "AB-102",
                            storyTitle: "Add Creation Page for Promotions",
                            points: 3
                        },
                        {
                            storyNumber: "AB-103",
                            storyTitle: "Add Edit Page for Promotions",
                            points: 2.5
                        }
                    ]
                }
            ]
        }
    }
});