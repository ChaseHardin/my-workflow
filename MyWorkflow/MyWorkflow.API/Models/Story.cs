namespace MyWorkflow.API.Models
{
    public class Story
    {
        public int StoryId { get; set; }
        public string StoryName { get; set; }
        public string StoryDescription { get; set; }
        public double StoryPoints { get; set; }
    }
}